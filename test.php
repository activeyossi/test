<?php

$data = file_get_contents('php://input');

if ($data) {
    $data = json_decode($data, true);

    $result = [];
    $user_yyy_score = [];
    foreach ($data as $row) {
        if ($row['type'] == 'yyy') {
            if ($user_yyy_score[$row['user']]) {
                if ($user_yyy_score[$row['user']]['row']['score'] < $row['score']) {
                    $user_yyy_score[$row['user']]['row'] = $row;
                }
            } else {
                $user_yyy_score[$row['user']]['row'] = $row;
            }
        } else {
            $result[] = $row;
        }
    }

    foreach ($user_yyy_score as $user) {
        $result[] = $user['row'];
    }

    //print json_decode($result)
    include 'output.php';
}