<html>
    <head>
        <style>
            body {
                background-color: cadetblue;
            }
            h3 {
                padding: 10px;
                background-color: #aefff0;
                color: #009696;
            }

            td {
                padding: 5px;
            }

            tr:nth-child(even) {
                background: #aefff0
            }
            tr:nth-child(odd) {
                background: #e8fffc
            }

        </style>
    </head>
    <body>
        <h3>Input</h3>

        <table>
            <tr>
                <td>id</td>
                <td>user</td>
                <td>score</td>
                <td>type</td>
            </tr>
            <?php foreach ($data as $row): ?>
                <tr>
                    <td><?=$row['_id']?></td>
                    <td><?=$row['user']?></td>
                    <td><?=$row['score']?></td>
                    <td><?=$row['type']?></td>
                </tr>
            <?php endforeach; ?>
        </table>

        <h3>Output</h3>

        <table>
            <tr>
                <td>id</td>
                <td>user</td>
                <td>score</td>
                <td>type</td>
            </tr>
            <?php foreach ($result as $row): ?>
                <tr>
                    <td><?=$row['_id']?></td>
                    <td><?=$row['user']?></td>
                    <td><?=$row['score']?></td>
                    <td><?=$row['type']?></td>
                </tr>
            <?php endforeach; ?>
        </table>
    </body>
</html>